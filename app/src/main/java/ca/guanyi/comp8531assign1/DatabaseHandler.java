package ca.guanyi.comp8531assign1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Guanyi on 5/4/2017.
 */

public class
DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "contactsDatabase.db";

    // Contacts table name
    private static final String TABLE_CONTACTS = "Contact";
    private static final String TRIGGER = "ValidateNameBeforeInsert";

    private static final String COL_EMAIL = "Email";
    private static final String COL_NAME = "Name";
    private static final String COL_PHONE_NUMBER = "PhoneNumber";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_CONTACTS + "("
                + COL_EMAIL + " TEXT PRIMARY KEY, " + COL_NAME + " TEXT, "
                + COL_PHONE_NUMBER + " TEXT" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);

        String CREATE_TRIGGER = "CREATE TRIGGER IF NOT EXISTS " + TRIGGER + " BEFORE INSERT ON " + TABLE_CONTACTS +
                " BEGIN SELECT CASE WHEN NEW.Email LIKE '%@%' THEN RAISE (ABORT, 'Invalid Name') END; END;";
        db.execSQL(CREATE_TRIGGER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);
        onCreate(db);
    }

    public void addContact(Contact contact) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COL_EMAIL, contact.getEmail());
        values.put(COL_NAME, contact.getName());
        values.put(COL_PHONE_NUMBER, contact.getPhoneNumber());

        db.insert(TABLE_CONTACTS, null, values);
        db.close();
    }

    // Getting All Contacts
    public String getAllContacts() {
        //List<Contact> contactList = new ArrayList<>();
        String content = "";
        String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Contact contact = new Contact();
//                contact.setEmail(cursor.getString(0));
//                contact.setName(cursor.getString(1));
//                contact.setPhoneNumber(cursor.getString(2));
                // Adding contact to list
                //contactList.add(contact);
                content = content + cursor.getString(0) + "  ";
                content = content + cursor.getString(1) + "   ";
                content = content + cursor.getString(2) + "\n";
            } while (cursor.moveToNext());
        }
        db.close();
        return content;
    }
}

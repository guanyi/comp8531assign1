package ca.guanyi.comp8531assign1;

/**
 * Created by Guanyi on 5/4/2017.
 */

public class Contact {

    private String _name;
    private String _email;
    private String _phone_number;

    public Contact(){}

    public Contact(String name, String email, String _phone_number){
        this._name = name;
        this._email = email;
        this._phone_number = _phone_number;
    }

    public String getName(){ return this._name; }

    public void setName(String name){ this._name = name; }

    public String getEmail(){ return this._email; }

    public void setEmail(String email){ this._email = email; }

    public String getPhoneNumber(){ return this._phone_number; }

    public void setPhoneNumber(String phone_number){ this._phone_number = phone_number; }
}

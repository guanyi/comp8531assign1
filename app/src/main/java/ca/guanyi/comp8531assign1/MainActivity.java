package ca.guanyi.comp8531assign1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText etEmail;
    EditText etName;
    EditText etPhone;
    Button btEnter;
    EditText etContent;
    DatabaseHandler dbHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etEmail = (EditText) findViewById(R.id.etEmail);
        etName = (EditText) findViewById(R.id.etName);
        etPhone = (EditText) findViewById(R.id.etPhone);
        btEnter = (Button) findViewById(R.id.btEnter);
        etContent = (EditText) findViewById(R.id.etContent);

        dbHandler = new DatabaseHandler(this);
        etContent.setText(dbHandler.getAllContacts());
        btEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dbHandler.addContact(new Contact(etEmail.getText().toString(),
                                                 etName.getText().toString(),
                                                 etPhone.getText().toString()
                                                 )
                                     );

                etContent.setText(dbHandler.getAllContacts());
            }
        });
    }
}
